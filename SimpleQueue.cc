
#include <new>
#include "SimpleQueue.hh"


SimpleQueue::SimpleQueue(int size)
{
    queueSize = size;
    numEntries = 0;
    head = 0;
    tail = 0;

    vv = new int[queueSize]; 
}

SimpleQueue::~SimpleQueue()
{
    delete []vv;
}

bool SimpleQueue::dequeue(int& item)
{
    bool returnStatus = false;
    if (!isEmpty()) {
        item = vv[tail];
        tail = ((tail + 1) % queueSize);
        --numEntries;
        returnStatus = true;
    }
    return returnStatus;
}

bool SimpleQueue::enqueue(int item)
{
    bool returnStatus = false;
    if (!isFull()) {
        vv[head] = item;
        head = ((head + 1) % queueSize);
        ++numEntries;
        returnStatus = true;
    }
    return returnStatus;
}


bool SimpleQueue::peek(int& item)
{
    bool returnStatus = false;
    if (!isFull()) {
        item = vv[head];
    }
    return returnStatus;
}

bool SimpleQueue::peekAt (int index, int& item)
{
    bool returnStatus = false;
    if (index >= 0 && index < queueSize) {
        item = vv[index];
        returnStatus = true;
    }
    return returnStatus;
}

int SimpleQueue::getTail()
{
    return tail;
}

bool SimpleQueue::isFull()
{
    return (numEntries == queueSize);
}


bool SimpleQueue::isEmpty()
{
    return (numEntries == 0);
}

int SimpleQueue::getNumEntries()
{
    return numEntries;
}


int SimpleQueue::getQueueSize()
{
    return queueSize;
}
