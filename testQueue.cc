#include <iostream>

#include "SimpleQueue.hh"

void printQueue(SimpleQueue &q)
{
    int index, tail;
    int queueSize = q.getQueueSize();
    int numPrinted = 0;
    int item;

    if (q.isEmpty()) {
        std::cout << "Queue is empty!" << std::endl;
        return;
    }

    index = tail = q.getTail();

    std::cout << "Queue: " ;

    for (int i = 0; i < index; i++) {
        std::cout << "- ";
    }
    
    while (true) {
        bool gotItem = q.peekAt(index, item);
        if (gotItem) {
            std::cout << item << " ";
            ++numPrinted;
        } else {
            break;
        }
        if (numPrinted == q.getNumEntries()){
            break;
        }

        index = (index + 1) % queueSize;
        // Queue is full, and we completed iterating it.
        if (index == tail) {
            break;
        }
    }
    std::cout << std::endl;
}

int main(int argc, char **argv)
{
    SimpleQueue q(20);
    int item;
    int enqueued = 0;

    for (int i = 0; i < 15; i++) {
        if (q.enqueue(++enqueued)) {
            std::cout << "enqueued: " << enqueued << std::endl;
        }else {
            std::cout << "enqueue(" << enqueued << ") failed! Q is full" << std::endl;
        }
    }
    printQueue(q);
    if (q.dequeue(item)) {
        std::cout << "dequeued " << item << std::endl;
    } else {
        std::cout << "dequeue - Queue Empty" << std::endl;
    }

    if (q.dequeue(item)) {
        std::cout << "dequeued " << item << std::endl;
    } else {
        std::cout << "dequeue - Queue Empty" << std::endl;
    }

    if (q.enqueue(++enqueued)) {
        std::cout << "enqueued: " << enqueued << std::endl;
    }else {
        std::cout << "enqueue(" << enqueued << ") failed! Q is full" << std::endl;
    }

    if (q.dequeue(item)) {
        std::cout << "dequeued " << item << std::endl;
    } else {
        std::cout << "dequeue - Queue Empty" << std::endl;
    }

    printQueue(q);

    if (q.dequeue(item)) {
        std::cout << "dequeued " << item << std::endl;
    } else {
        std::cout << "dequeue - Queue Empty" << std::endl;
    }

    printQueue(q);

    if (q.enqueue(++enqueued)) {
        std::cout << "enqueued: " << enqueued << std::endl;
    }else {
        std::cout << "enqueue(" << enqueued << ") failed! Q is full" << std::endl;
    }
    if (q.enqueue(++enqueued)) {
        std::cout << "enqueued: " << enqueued << std::endl;
    }else {
        std::cout << "enqueue(" << enqueued << ") failed! Q is full" << std::endl;
    }
    printQueue(q);
    if (q.enqueue(++enqueued)) {
        std::cout << "enqueued: " << enqueued << std::endl;
    }else {
        std::cout << "enqueue(" << enqueued << ") failed! Q is full" << std::endl;
    }
    if (q.enqueue(++enqueued)) {
        std::cout << "enqueued: " << enqueued << std::endl;
    }else {
        std::cout << "enqueue(" << enqueued << ") failed! Q is full" << std::endl;
    }

    printQueue(q);

    if (q.dequeue(item)) {
        std::cout << "dequeued " << item << std::endl;
    } else {
        std::cout << "dequeue - Queue Empty" << std::endl;
    }

    if (q.dequeue(item)) {
        std::cout << "dequeued " << item << std::endl;
    } else {
        std::cout << "dequeue - Queue Empty" << std::endl;
    }

    if (q.dequeue(item)) {
        std::cout << "dequeued " << item << std::endl;
    } else {
        std::cout << "dequeue - Queue Empty" << std::endl;
    }
    printQueue(q);
    if (q.dequeue(item)) {
        std::cout << "dequeued " << item << std::endl;
    } else {
        std::cout << "dequeue - Queue Empty" << std::endl;
    }

    if (q.enqueue(++enqueued)) {
        std::cout << "enqueued: " << enqueued << std::endl;
    }else {
        std::cout << "enqueue(" << enqueued << ") failed! Q is full" << std::endl;
    }
    if (q.enqueue(++enqueued)) {
        std::cout << "enqueued: " << enqueued << std::endl;
    }else {
        std::cout << "enqueue(" << enqueued << ") failed! Q is full" << std::endl;
    }
    
    printQueue(q);
    if (q.dequeue(item)) {
        std::cout << "dequeued " << item << std::endl;
    } else {
        std::cout << "enqueue(" << enqueued << ") failed! Q is full" << std::endl;
        std::cout << "dequeue - Queue Empty" << std::endl;
    }
    if (q.dequeue(item)) {
        std::cout << "dequeued " << item << std::endl;
    } else {
        std::cout << "dequeue - Queue Empty" << std::endl;
    }
    printQueue(q);

    if (q.enqueue(++enqueued)) {
        std::cout << "enqueued: " << enqueued << std::endl;
    }else {
        std::cout << "enqueue(" << enqueued << ") failed! Q is full" << std::endl;
    }
    if (q.enqueue(++enqueued)) {
        std::cout << "enqueued: " << enqueued << std::endl;
    }else {
        std::cout << "enqueue(" << enqueued << ") failed! Q is full" << std::endl;
    }
    printQueue(q);

    if (q.dequeue(item)) {
        std::cout << "dequeued " << item << std::endl;
    } else {
        std::cout << "dequeue - Queue Empty" << std::endl;
    }
    if (q.dequeue(item)) {
        std::cout << "dequeued " << item << std::endl;
    } else {
        std::cout << "dequeue - Queue Empty" << std::endl;
    }
    printQueue(q);
}
