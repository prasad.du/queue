/*
* SimpleQueue - demonstrating functionality of a simple circular queue.
* 
*/

#ifndef _SIMPLE_QUEUE_H_
#define _SIMPLE_QUEUE_H_

#include <vector>

class SimpleQueue
{
    private:
        int *vv;
        int numEntries;
        int queueSize;
        int head, tail;

    public:
        SimpleQueue(int queueSize);
        ~SimpleQueue();
        /*
         * Remove the item from head of queue and place the removed item in item.
         * Return true if there was an item in the queue to dequeue,
         * false otherwise
         */
        bool dequeue(int& item);
        /*
         * Ann item to tail of queue, and return true.
         * If queue is full, item is not inserted, return false.
         */
        bool enqueue(int item); 
        /*
         * Place the item at head of queue without dequeuing it into item.
         * Return true if queue is not true.
         * false otherwise.
         */
        bool peek(int& item);
        
        /*
         * plce the item at index into 'item'
         * return true if index is within queue size.
         * false otherwise.
         */
        bool peekAt (int index, int& item);

        /*
         * return current tail position. Useful for printing the Queue.
         */
        int getTail();

        /*
         * return true if queue is full. No more items can be enqueued.
         * return false otherwise.
         */
        bool isFull();
        /*
         * return true if queue is empty. No more items to dequeue.
         * return false otherwise.
         */
        bool isEmpty();
        /*
         * return count of items in the queue.
         */
        int  getNumEntries();
        /*
         * return size (how many items can the queue fit?) of the queue.
         */
        int  getQueueSize();
};

#endif
