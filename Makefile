CC := g++
CPPFLAGS := -g -std=c++14

testQueue	: SimpleQueue.o
SimpleQueue.o : SimpleQueue.cc


clean:
	rm -f *.o
	rm -f testQueue
